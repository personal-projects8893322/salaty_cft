"""
An optional module for creating and populating the database. Can be used to
store your own data. When creating a new database make sure to delete the old one
"""

from sqlalchemy import create_engine
from sqlalchemy import MetaData, Table, Column, Integer, String, Boolean, ForeignKey

from passlib.context import CryptContext
from dotenv import dotenv_values


# Path to the database
db_url = dotenv_values(dotenv_path='.env')["DB_URL"]
engine = create_engine(db_url) # type: ignore
meta_data = MetaData()

# Table structure
users = Table(
    'user_data', meta_data,
    Column('id', Integer, primary_key=True),
    Column('username', String, nullable=False, unique=True),
    Column('full_name', String),
    Column('email', String, nullable=False, unique=True),
    Column('hashed_password', String, nullable=False),
    Column('disabled', Boolean)
    )

salaries = Table(
    'salary_data', meta_data,
    Column('id', Integer, primary_key=True),
    Column('current_salary', Integer),
    Column('expected_promotion', String),
    Column('user_id', Integer, ForeignKey("user_data.id"), unique=True)
    )

meta_data.create_all(engine)

# The database of users in dict form with unhashed passwords
fake_people = {
    "michaelscott": {
        "username": "michaelscott",
        "full_name": "Michael Scott",
        "email": "michaelscott@dundermifflin.com",
        "hashed_password": "secret",
        "disabled": False
    },
    "dwightschrute": {
        "username": "dwightschrute",
        "full_name": "Dwight Schrute",
        "email": "dwightschrute@dundermifflin.com",
        "hashed_password": "bear",
        "disabled": False
    },
    "jimhalpert": {
        "username": "jimhalpert",
        "full_name": "Jim Halpert",
        "email": "jimhalpert@dundermifflin.com",
        "hashed_password": "pam",
        "disabled": False
    },
    "pambeesly": {
        "username": "pambeesly",
        "full_name": "Pam Beesly",
        "email": "pambeesly@dundermifflin.com",
        "hashed_password": "jim",
        "disabled": False
    }
}

# Hash all the passwords using the same method as in the main.py
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

for user in fake_people.values():
    password = user["hashed_password"]
    user.update({"hashed_password": pwd_context.hash(password)})

# The database of salary data in dict form
fake_salaries = {
    "michaelscott": {
        "current_salary": 100,
        "expected_promotion": "2024.10.14",
        "user_id": 1
    },
    "dwightschrute": {
        "current_salary": 10,
        "expected_promotion": "2025.02.30",
        "user_id": 2
    },
    "jimhalpert": {
        "current_salary": 150,
        "expected_promotion": "2024.09.16",
        "user_id": 3
    },
    "pambeesly": {
        "current_salary": 140,
        "expected_promotion": "2024.10.20",
        "user_id": 4
    }
}

conn = engine.connect()

# Populate the database using 'INSERT OR IGNORE' SQL methods
for user in fake_people.values():
    stmt = users.insert().values(**user).prefix_with('OR IGNORE')
    result = conn.execute(stmt)

for salary in fake_salaries.values():
    stmt = salaries.insert().values(**salary).prefix_with('OR IGNORE')
    result = conn.execute(stmt)

conn.commit()