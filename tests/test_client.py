"""
Tests for all the funtions of 'client.py'
The 'main()' function isn't tested because it's just a placeholder
for a more robust user interface
"""

import pytest
import client
import unittest.mock as mock
import requests

@pytest.fixture()
def user_data():
    """
    A dict of user data returned by the API
    """
    user_data = {
        "id": 1,
        "username": "michaelscott",
        "full_name": "Michael Scott",
        "email": "michaelscott@dundermifflin.com",
        "hashed_password": "secret",
        "disabled": False
        }
    return user_data


@pytest.fixture()
def salary_data():
    """
    A dict of salary data returned by the API
    """
    salary_data = {
        "username": "michaelscott",
        "current_salary": 100,
        "expected_promotion": "2024.10.14",
    }
    return salary_data


@pytest.fixture()
def mocked_url():
    with mock.patch('client.BaseURL.get_base_url', return_value="http://url.com"):
        yield


@pytest.fixture()
def mocked_requests_get():
    """
    Generate a mock of a 'requests.get()' object
    """
    with mock.patch.object(requests, 'get') as mocked:
        mocked_requests_response = mock.Mock()
        mocked.return_value = mocked_requests_response
        yield mocked_requests_response


@pytest.fixture()
def mocked_requests_post():
    """
    Generate a mock of a 'requests.post()' object
    """
    with mock.patch.object(requests, 'post') as mocked:
        mocked_requests_response = mock.Mock()
        mocked.return_value = mocked_requests_response
        yield mocked_requests_response


def test_authenticate(mocked_requests_post, mocked_url):
    """
    Test 'authenticate()' with a valid username and password
    """
    # Create a mocked response of a POST command
    mocked_requests_post.status_code = 200
    mocked_requests_post.json.return_value = {"access_token": "access_token"}

    # Call the function with suitable username and password
    access_code = client.authenticate("username", "password")

    assert access_code == {"access_token": "access_token"}


def test_authenticate_wrong(mocked_requests_post, mocked_url):
    """
    Test 'authenticate()' with wrong username or password
    """
    # Create a mocked response of a POST command
    # Technicaly it's not needed but I'll keep it in case I choose to display errors differently
    mocked_requests_post.status_code = 401
    mocked_requests_post.json.return_value = {"detail": "Incorrect username or password"}
    # Mock a 'raise_for_status()' behavior when given an error code
    mocked_requests_post.raise_for_status.side_effect = requests.HTTPError

    # Call the function with wrong username or password
    access_code = client.authenticate("wrong", "wrong")

    assert access_code == False


def test_authenticate_not_str():
    """
    Test 'authenticate()' with username or passwords which aren't even str
    """
    # Define username and password of some random types
    username = 10
    password = {"key": "item"}

    # Call the function with not str username and password
    access_code = client.authenticate(username, password) # type: ignore

    assert access_code == False


def test_get_user(mocked_requests_get, user_data, mocked_url):
    """
    Test 'get_item()' with a valid type for user data and access token
    """
    # Create a mocked response of a GET command
    mocked_requests_get.status_code = 200
    mocked_requests_get.json.return_value = user_data

    # Define suitable 'type' and 'access_token' variables
    type = "current_user"
    access_token = {"token_type": "any", "access_token": "any"}
    # Call the tested function
    result = client.get_item(type, access_token)

    assert result == user_data


def test_get_item_invalid_token(mocked_requests_get, mocked_url):
    """
    Test 'get_item()' with an invalid access token
    """
    # Create a mocked response of a GET command
    # Technicaly it's not needed but I'll keep it in case I choose to display errors differently
    mocked_requests_get.status_code = 401
    # Get an expected dict from the fixture
    mocked_requests_get.json.return_value = {"detail": "Could not validate credentials"}
    # Mock a 'raise_for_status()' behavior when given an error code
    mocked_requests_get.raise_for_status.side_effect = requests.HTTPError

    # Define suitable 'type' and 'access_token' variables
    # Type could be any
    type = "current_user"
    access_token = {"token_type": "any", "access_token": "any"}
    # Call the tested function
    user_data = client.get_item(type, access_token)

    assert user_data == False


def test_get_item_invalid_type():
    """
    Test 'get_item()' with an invalid type variable
    """
    # Define a wrong type and any access token
    type = "wrong"
    access_token = {"token_type": "any", "access_token": "any"}
    # Call the tested function
    user_data = client.get_item(type, access_token)

    assert user_data == False


@pytest.mark.parametrize("access_token",
                         [{"wrong": "any", "more_wrong": "any"}, 10, "boop"])
def test_get_item_invalid_token_dict(access_token):
    """
    Test 'get_item()' with an invalid token structure
    """
    # Define any type and wrong access token
    type = "current_user"
    # Call the tested function
    user_data = client.get_item(type, access_token)

    assert user_data == False


def test_get_salary(mocked_requests_get, salary_data, mocked_url):
    """
    Test 'get_item()' with a valid type for salary data and access token
    """
    # Create a mocked response of a GET command
    mocked_requests_get.status_code = 200
    # Get an expected dict from the fixture
    mocked_requests_get.json.return_value = salary_data

    # Define suitable 'type' and 'access_token' variables
    type = "salary"
    access_token = {"token_type": "any", "access_token": "any"}
    # Call the tested function
    result = client.get_item(type, access_token)

    assert result == salary_data
