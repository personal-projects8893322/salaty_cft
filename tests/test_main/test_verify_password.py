"""
Tests for 'main.verify_password_function()'
"""

import pytest
import main
import unittest.mock as mock
from fastapi import HTTPException
from passlib.context import CryptContext

@pytest.fixture()
def pwd_context():
    return CryptContext(schemes=["bcrypt"], deprecated="auto")


@pytest.fixture()
def mocked_setup():
    """
    Generate a mock of a 'main.Setup' object
    """
    with mock.patch.object(main, 'Setup') as mocked:
        mocked_setup_object = mock.Mock()
        mocked.return_value = mocked_setup_object
        yield mocked_setup_object


def test_verify_password(mocked_setup, pwd_context):
    """
    Test 'verify_password()' with a correct password 
    """
    # Mock 'get_pwd_context()' method to poduce a CryptoContext object
    mocked_setup.get_pwd_context.return_value = pwd_context
    # Hash the plain password to check
    plain_password = "password"
    hashed_password = pwd_context.hash(plain_password)
    # Call 'verify_password()' with correct password pair
    result = main.verify_password(plain_password, hashed_password)

    assert result == True


def test_verify_password_wrong(mocked_setup, pwd_context):
    """
    Test 'verify_password()' with an incorrect password 
    """
    # Mock 'get_pwd_context()' method to poduce a CryptoContext object
    mocked_setup.get_pwd_context.return_value = pwd_context
    # Hash the plain password incorrectly to check
    plain_password = "password"
    hashed_password = pwd_context.hash(plain_password + "boop")
    # Call 'verify_password()' with an incorrect password pair
    result = main.verify_password(plain_password, hashed_password)

    assert result == False


@pytest.mark.parametrize("hashed_password",
                         [10, {"key": "value"}, ["beep", "boop"]])
def test_verify_password_invalid(hashed_password):
    """
    Test 'verify_password()' with a hashed password of an incorrect type
    """
    # Assume that the plain password is correct since it's already checked by 'autheticate_user()'
    password = "correct"
    # Call 'verify_password()' with a hashed password that's not str
    with pytest.raises(HTTPException):
        result = main.verify_password(password, hashed_password)
