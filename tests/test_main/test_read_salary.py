"""
Tests for 'main.read_salary()' function.
"""

import pytest
import main
from fastapi.testclient import TestClient
import unittest.mock as mock
from datetime import timedelta, datetime, timezone
import jwt

client = TestClient(main.app)

@pytest.fixture()
def salary_data():
    """
    A dict of salary data returned by the API
    """
    salary_data = {
        "username": "michaelscott",
        "current_salary": 100,
        "expected_promotion": "2024.10.14",
    }
    return salary_data


@pytest.fixture()
def mocked_get_database():
    """
    Generate a mock of a 'main.Setup.get_database()' method
    """
    with mock.patch.object(main.Setup, 'get_database') as mocked:
        mocked_database_object = mock.Mock()
        mocked.return_value = mocked_database_object
        yield mocked_database_object


@pytest.fixture()
def access_token():
    """
    Create a valid access token
    """
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    data = {"sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)

    return access_token


def test_read_salary(mocked_get_database, access_token, salary_data):
    """
    Test 'get_salary()' function with a correct GET command
    """

    def user_data_override():
        """
        A function to override 'get_users_me()' dependency on 'get_current_user()'
        """
        return {"username": "username"}

    # Override the 'get_current_user()' dependency
    main.app.dependency_overrides[main.get_current_user] = user_data_override

    # Mock the database response
    mocked_get_database.get_salary.return_value = salary_data

    # Construct a correct GET command
    path = "/users/me/salary"
    headers = {'accept': 'application/json', 'Authorization': access_token}
    response = client.get(path, headers=headers)

    # Put the dependency back where it belongs
    main.app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == salary_data


def test_read_salary_no_money(mocked_get_database, access_token):
    """
    Test 'get_salary()' function if there's no salary data in the DB
    """

    def user_data_override():
        """
        A function to override 'get_users_me()' dependency on 'get_current_user()'
        """
        return {"username": "username"}

    # Override the 'get_current_user()' dependency
    main.app.dependency_overrides[main.get_current_user] = user_data_override

    # Mock the database response
    mocked_get_database.get_salary.return_value = False

    # Construct a correct GET command
    path = "/users/me/salary"
    headers = {'accept': 'application/json', 'Authorization': access_token}
    response = client.get(path, headers=headers)

    # Put the dependency back where it belongs
    main.app.dependency_overrides = {}

    assert response.status_code == 404


def test_read_salary_wrong_path(access_token):
    """
    Test 'get_salary()' function with an incorrect path
    """
    # Construct a correct GET command
    path = "/boop"
    headers = {'accept': 'application/json', 'Authorization': access_token}
    response = client.get(path, headers=headers)

    assert response.status_code == 404


def test_read_salary_wrong_token(access_token):
    """
    Test 'get_users_me()' function with an incorrect access token
    """
    # Construct a correct GET command
    path = "/users/me/salary"
    headers = {'accept': 'application/json', 'Authorization': "boop"}
    response = client.get(path, headers=headers)

    assert response.status_code == 401
