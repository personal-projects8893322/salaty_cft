"""
Test for 'main.authenticate_user()' function
Since 'main.verify_password()' is already tested, it will be mocked here
"""

import pytest
import main
import unittest.mock as mock
from fastapi import HTTPException

@pytest.fixture()
def user_data():
    """
    A dict of user data returned by the API
    """
    user_data = {
        "id": 1,
        "username": "michaelscott",
        "full_name": "Michael Scott",
        "email": "michaelscott@dundermifflin.com",
        "hashed_password": "hased_secret",
        "disabled": False
        }
    return user_data


@pytest.fixture()
def salary_data():
    """
    A dict of salary data returned by the API
    """
    salary_data = {
        "username": "michaelscott",
        "current_salary": 100,
        "expected_promotion": "2024.10.14",
    }
    return user_data


@pytest.fixture()
def mocked_get_database():
    """
    Generate a mock of a 'main.Setup.get_database()' method
    """
    with mock.patch.object(main.Setup, 'get_database') as mocked:
        mocked_database_object = mock.Mock()
        mocked.return_value = mocked_database_object
        yield mocked_database_object


@mock.patch('main.verify_password')
def test_authenticate_user(mocked_verify, mocked_get_database, user_data):
    """
    Test 'authenticate_user()' with correct username and password
    """
    # Mock 'get_user()' return dict as if the username is correct
    mocked_get_database.get_user.return_value = user_data
    # Define a valid username and password
    username = "michaelscott"
    password = "hashed_secret"
    # Mock 'verify_password()' return value
    mocked_verify.return_value = True

    # Call 'authenticate_user()' with correct username and password
    result = main.authenticate_user(username, password)

    assert result == user_data
    

@mock.patch('main.verify_password')
def test_authenticate_user_wrong_username(mocked_verify, mocked_get_database):
    """
    Test 'authenticate_user()' with an incorrect username
    """
    # Mock 'get_user()' return dict as if the username is incorrect
    mocked_get_database.get_user.return_value = False
    # Define a invalid username and password
    username = "michael_NOT_scott"
    password = "hashed_secret"
    # Mock 'verify_password()' return value. Let's say it's somehow valid
    mocked_verify.return_value = True
    # Call 'authenticate_user()' with an incorrect username and correct password
    result = main.authenticate_user(username, password)

    assert result == False


@mock.patch('main.verify_password')
def test_authenticate_user_wrong_password(
    mocked_verify, mocked_get_database, user_data):
    """
    Test 'authenticate_user()' with an incorrect password
    """
    # Mock 'get_user()' return dict as if the username is incorrect
    mocked_get_database.get_user.return_value = user_data
    # Define a invalid username and password
    username = "michaelscott"
    password = "wrong"
    # Mock 'verify_password()' return value
    mocked_verify.return_value = False
    # Call 'authenticate_user()' with correct username and incorrect password
    result = main.authenticate_user(username, password)

    assert result == False


@pytest.mark.parametrize(['username', 'password'],
                         [(1, 1), ([1], [1]), ({'1': '1'}, {'1': '1'})])
def test_authenticate_user_not_str(username, password):
    """
    Test 'authenticate_user()' with username and password of incorrect types
    """
    # Call 'authenicate_user()' with username and password that aren't strings
    with pytest.raises(HTTPException):
        result = main.authenticate_user(username, password)
