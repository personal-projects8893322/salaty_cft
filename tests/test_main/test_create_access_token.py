"""
Tests for 'main.create_access_token()' function.
It's the only test I could come up with since all the variables are provided by
other functions and thereby should be correct
"""

import pytest
import main
import unittest.mock as mock
from datetime import timedelta, datetime, timezone
import jwt

@pytest.fixture()
def expires_delta():
    """
    Create a timedelta object
    """
    return timedelta(minutes=1)


@pytest.fixture()
def mocked_environment():
    """
    Generate a mock of a 'main.Enviroment' methods
    """
    with mock.patch('main.Environment.get_SECRET_KEY', return_value="super_secret"):
        with mock.patch('main.Environment.get_ALGORITHM', return_value="HS256"):
            yield


def test_create_access_token(mocked_environment, expires_delta):
    """
    Test 'create_access_token()' with correct data and expires_delta
    """
    # Calculate the expitation time
    expire = datetime.now(timezone.utc) + expires_delta
    # Create a correct data dict
    data = {"sub": "username", "exp": expire}

    # Define the test enviroment variables the same as the mocked ones
    secret_key = "super_secret"
    algorithm = "HS256"

    # Create an encoded string to check with
    encoded_jwt = jwt.encode(data, secret_key, algorithm=algorithm)
    # Call the 'create_access_token()' function with correct data and expires_delta
    result = main.create_access_token(data, expires_delta)

    assert result == encoded_jwt
