"""
Tests for 'main.read_users_me()' function.
"""

import pytest
import main
from fastapi.testclient import TestClient
from datetime import timedelta, datetime, timezone
import jwt

client = TestClient(main.app)


@pytest.fixture()
def user_data():
    """
    A dict of user data returned by the API
    """
    user_data = {
        "id": 1,
        "username": "michaelscott",
        "full_name": "Michael Scott",
        "email": "michaelscott@dundermifflin.com",
        "hashed_password": "hased_secret",
        "disabled": False
        }
    return user_data


@pytest.fixture()
def access_token():
    """
    Create a valid access token
    """
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    data = {"sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)

    return access_token


def test_read_users_me(access_token, user_data):
    """
    Test 'get_users_me()' function with a correct GET command
    """

    def user_data_override():
        """
        A function to override 'get_users_me()' dependency on 'get_current_user()'
        """
        return user_data

    # Override the 'get_current_user()' dependency
    main.app.dependency_overrides[main.get_current_user] = user_data_override

    # Construct a correct GET command
    path = "/users/me"
    headers = {'accept': 'application/json', 'Authorization': access_token}
    response = client.get(path, headers=headers)

    # Put the dependency back where it belongs
    main.app.dependency_overrides = {}

    assert response.status_code == 200
    assert response.json() == user_data


def test_read_users_me_wrong_path(access_token):
    """
    Test 'get_users_me()' function with an incorrect path
    """
    # Construct a correct GET command
    path = "/boop"
    headers = {'accept': 'application/json', 'Authorization': access_token}
    response = client.get(path, headers=headers)

    assert response.status_code == 404


def test_read_users_me_wrong_token(access_token):
    """
    Test 'get_users_me()' function with an incorrect access token
    """
    # Construct a correct GET command
    path = "/users/me"
    headers = {'accept': 'application/json', 'Authorization': "boop"}
    response = client.get(path, headers=headers)

    assert response.status_code == 401