"""
Tests for 'main.login_for_access_token()' function.
"""

import pytest
import unittest.mock as mock
import main
from fastapi.testclient import TestClient

client = TestClient(main.app)


@pytest.fixture()
def mocked_environment():
    """
    Generate a mock of a 'main.Enviroment' methods
    """
    with mock.patch('main.Environment.get_EXPIRE', return_value=1):
        yield


@pytest.fixture()
def mocked_create_access_token():
    """
    Generate a mock of a 'main.create_access_token' function
    """
    with mock.patch('main.create_access_token', return_value="boop") as mocked:
        yield mocked


@mock.patch("main.authenticate_user", return_value = {"username": "username"})
def test_login_for_access_token(
    mocked_authenticate_user, mocked_environment, mocked_create_access_token):
    """
    Test 'login_for_access_token()' with correct username and password
    """
    # Create a valid POST command
    path = "/token"
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    login_data = {"username": "username", "password": "password"}
    response = client.post(path, headers=headers, data=login_data)
    # Create an expected result
    expected_token_json = {"access_token": "boop", "token_type": "bearer"}
    assert response.status_code == 200
    assert response.json() == expected_token_json


@mock.patch("main.authenticate_user", return_value = False)
def test_login_for_access_token_not_authenticated(mocked_authenticate_user):
    """
    Test 'login_for_access_token()' with an incorrect username and password
    """
    # Create a valid POST command
    path = "/token"
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    login_data = {"username": "username", "password": "password"}
    response = client.post(path, headers=headers, data=login_data)

    assert response.status_code == 401


def test_login_for_access_incorrect_path():
    """
    Test 'login_for_access_token()' with an incorrect path
    """
    # Create a POST command with an incorrect path
    path = "/boop"
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    login_data = {"username": "username", "password": "password"}
    response = client.post(path, headers=headers, data=login_data)

    assert response.status_code == 404


def test_login_for_access_login_data_incorrect_data():
    """
    Test 'login_for_access_token()' with incorrect data
    """
    # Create a POST command with a incorrect data
    path = "/token"
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    login_data = {"beep": "boop"}
    response = client.post(path, headers=headers, data=login_data)

    assert response.status_code == 422