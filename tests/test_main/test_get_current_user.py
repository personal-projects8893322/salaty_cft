"""
Tests for 'main.get_current_user()' function.
"""

import pytest
import main
import unittest.mock as mock
from datetime import timedelta, datetime, timezone
import jwt
from fastapi import HTTPException


@pytest.fixture()
def mocked_environment():
    """
    Generate a mock of a 'main.Enviroment' methods
    """
    with mock.patch('main.Environment.get_SECRET_KEY', return_value="super_secret"):
        with mock.patch('main.Environment.get_ALGORITHM', return_value="HS256"):
            yield


@pytest.fixture()
def user_data():
    """
    A dict of user data returned by the API
    """
    user_data = {
        "id": 1,
        "username": "michaelscott",
        "full_name": "Michael Scott",
        "email": "michaelscott@dundermifflin.com",
        "hashed_password": "hased_secret",
        "disabled": False
        }
    return user_data


@pytest.fixture()
def mocked_get_database():
    """
    Generate a mock of a 'main.Setup.get_database()' method
    """
    with mock.patch.object(main.Setup, 'get_database') as mocked:
        mocked_database_object = mock.Mock()
        mocked.return_value = mocked_database_object
        yield mocked_database_object


def test_get_current_user(mocked_environment, mocked_get_database, user_data):
    """
    Test 'get_current_user()' with correct access token
    """
    # Create a valid token
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    data = {"sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)
    # Get a mock of user dict
    mocked_get_database.get_user.return_value = user_data
    # Call 'get_current_user()' with a valid access token
    result = main.get_current_user(access_token)

    assert result == user_data


@pytest.mark.parametrize('access_token', ["please", 10, {"key": "values"}])
def test_get_current_user_wrong(mocked_environment, access_token):
    """
    Test 'get_current_user()' with a completely wrong access token
    """
    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)


def test_get_current_user_wrong_subject(mocked_environment):
    """
    Test 'get_current_user()' with an access token with a wrong subject
    The "exp" field cannot have a different key, as it would raise a TypeError
    on the encoding step
    """
    # Create a token with a wrong subject
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    # It's wrong here
    data = {"not_sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)

    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)


def test_get_current_user_no_expiration(mocked_environment):
    """
    Test 'get_current_user()' with an access token with no expiration
    """
    # Create a token with no expiration
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    # It's wrong here
    data = {"sub": "michaelscott"}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)

    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)


def test_get_current_user_expired(mocked_environment):
    """
    Test 'get_current_user()' with an expired access token
    """
    # Create a an expired token
    expire = datetime.now(timezone.utc) - timedelta(minutes=100)
    data = {"sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)

    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)


def test_get_current_unknown_user(mocked_environment, mocked_get_database):
    """
    Test 'get_current_user()' with a username that's not in the database
    """
    # Create a valid token with an incorrect username
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    # It's wrong here
    data = {"sub": "not_michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)
    # Mock a False response from a DB since a username doesn't exist
    mocked_get_database.get_user.return_value = False

    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)


def test_get_current_disabled_user(mocked_environment, mocked_get_database, user_data):
    """
    Test 'get_current_user()' with a disabled user
    """
    # Create a valid token
    expire = datetime.now(timezone.utc) + timedelta(minutes=1)
    data = {"sub": "michaelscott", "exp": expire}
    secret_key = "super_secret"
    algorithm = "HS256"
    access_token = jwt.encode(data, secret_key, algorithm=algorithm)
    # Make it so the user is disabled
    user_data.update({"disabled": True})
    # Mock a DB user entry with a disabled user
    mocked_get_database.get_user.return_value = user_data

    # Call 'get_current_user()' with an incorrect access token
    with pytest.raises(HTTPException):
        result = main.get_current_user(access_token)
