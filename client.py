"""
Client application for interfacing with the api
"""

import requests
from dotenv import dotenv_values

class BaseURL():
    """
    Define a class with constant variables the same way it's done in the 'main.py'
    It allows to do tests without getting an error on import
    """
    def __init__(self):
        # Get the base url from the .env file
        self.env_variables = dotenv_values(".env")
        self.BASE_URL = None

    def get_base_url(self):
        if self.BASE_URL is None:
            # Base url for constrcting html requests. Needs to be the same as in the uvicorn server
            self.BASE_URL = self.env_variables["BASE_URL"]
        return self.BASE_URL
    

base_url = BaseURL()


def authenticate(username: str, password: str):
    """
    Sends an authentication POST command
    :param username: Username
    :param password: Password
    :return: JSON response if login is successfull or False if not
    """
    # Check the data types
    if not isinstance(username, str) or not isinstance(password, str):
        print("Username and password must be strings")
        return False
    
    # Constructing requests object
    path = "/token"
    headers = {'accept': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded'}
    login_data = {"username": username, "password": password}
    # Send a POST command
    page = requests.post(
        base_url.get_base_url() + path, headers=headers, data=login_data) # type: ignore

    try:
        # Try to raise an error
        page.raise_for_status()

    except requests.exceptions.HTTPError:
        # Print out the error
        print(page.json()["detail"])
        return False
    
    return page.json()


def get_item(type: str, access_token: dict):
    """
    Sends a GET command for fetching data
    :param type: Type of data: "current_user", "salary"
    :param access_token: Access token json dict from a successful login response
    :return: JSON response if data is fetched or False if not
    """
    # Dict of available data types
    paths = {"current_user": "/users/me", "salary": "/users/me/salary/"}
    if type not in paths.keys():
        return False
    
    # Check for a KeyError just in case something went wrong
    try:
        access_string = access_token['token_type'] + ' ' + access_token['access_token']
    except (KeyError, TypeError):
        print("Access key is corrupted!")
        return False
    
    # Constructing a GET command    
    current_path = paths[type]
    headers = {'accept': 'application/json', 'Authorization': access_string}  
    # Send a GET command
    page = requests.get(
        url=base_url.get_base_url() + current_path, headers=headers) # type: ignore
    try:
        # Try to raise an error
        page.raise_for_status()
    
    except requests.exceptions.HTTPError:
        # Print out the error
        print(page.json()["detail"])
        return False
    
    return page.json()


def main():
    """
    Basic command line interface for interacting with the server
    """
    username = None
    password = None
    access_token = None
    user_data = None
    
    while True:
        # If there is no access token log in the user
        if not access_token:
            print("Welcome to DunderMifflin! Please log in")
            username = input("Username: ")
            password = input("Password: ")

            # Protection from an empty username and password
            if not username or not password:
                print("Username or password can't be empty!")
                continue
            
            access_token = authenticate(username, password)
            # Clear the user data
            user_data = None
        # If there is an access token try fetching the data
        else:
            # If there is user data don't fetch it again
            if not user_data:
                user_data = get_item("current_user", access_token)
                # If the user data came back as False print out an error and clear the access toke
                if not user_data:
                    access_token = None
                    continue

            print("\nWelcome,", user_data["full_name"])
            print("Please select an option:\n",
                    "1. Check user data\n",
                    "2. Check my salary\n",
                    "3. Log out")
            
            ans = input()

            if ans == "1":

                for key in user_data.keys():
                    if key == "id" or key == "hashed_password": continue
                    print(key, ":", user_data[key])
                input("\nPress any key...")

            elif ans == "2":
                salary_data = get_item("salary", access_token)
                if salary_data is not False:
                    for key in salary_data.keys():
                        if key == "username": continue
                        print(key, ":", salary_data[key])
                else:
                    print("Salary data is corrupted. Please contact the system administrator")
                input("\nPress any key...")

            elif ans == "3":
                access_token = None
            
            else:
                print("Please select a correct option (1-3)")
                continue
    

if __name__ == '__main__':
    main()
