"""
Module for interacting with the database
"""

from sqlalchemy import create_engine, select
from sqlalchemy import MetaData
from dotenv import dotenv_values

class DB_interface():
    """
    Database object with methods to access the data
    """
    def __init__(self):
        # Reflect an existing database scheme
        db_url = dotenv_values('.env')["DB_URL"]
        self.engine = create_engine(db_url) # type: ignore
        self.meta_data = MetaData()
        MetaData.reflect(self.meta_data, bind=self.engine)

        # Set up SQLalchemy Table objects
        self.users = self.meta_data.tables['user_data']
        self.salaries = self.meta_data.tables['salary_data']
        self.conn = self.engine.connect()


    def get_user(self, user_name: str):
        """
        Fetch a row from user_data table filtered by the given username
        :param user_name: username stored in the database
        :return: row of the corresponding user placed into a dict or False if the username doesn't exist
        """
        # Form and execute a statement to get the row from db
        stmt = select(self.users).filter_by(username=user_name)
        user_data = self.conn.execute(stmt)

        if user_data is not None:
            # Form a dictionary
            for row_dict in user_data.mappings():
                return row_dict
        else:
            return False

    def get_salary(self, user_name: str):
        """
        Fetch a row from salary_data table filtered by the given username
        :param user_name: username stored in the database
        :return: row from the salary_data table and the username from the user_data table
        of the corresponding user placed into a dict or False if the username doesn't exist
        """
        # Form and execute a statement to get the row from db
        stmt = select(self.users.c.username, self.salaries.c.current_salary, self.salaries.c.expected_promotion)
        stmt = stmt.filter_by(username=user_name)
        # Join the data from two tables based on the user.id
        stmt = stmt.join_from(self.users, self.salaries, self.users.c.id == self.salaries.c.user_id)    
        salary_data = self.conn.execute(stmt)

        if salary_data is not None:
            # Form a dictionary
            for row_dict in salary_data.mappings():
                return row_dict
        else:
            return False
